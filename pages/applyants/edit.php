<?php
 $fields = [];
 $messages = [];

 $id = $_GET['modelid'];

if (isset($_POST['action'])) {

    $response = validateApplyantFields();

    $fields = $response[0];
    $messages = $response[1];

    if(sizeof($messages) == 0 ){
        $result = updateRecord("applyants" ,$_POST['modelid'], $fields);
    }else{
        $result = false;
    }
    if($result){
        $messages = ["Registro fue actualizado de manera exitosa!"];
        $class = "success";

    }else {
        $class = "error";
    }

}else {

    $record = selectRecordByID("applyants",$_GET['modelid']);

    $_POST['firstname'] = $record['firstname'];
    $_POST['lastname'] = $record['lastname'];
    $_POST['email'] = $record['email'];
    $_POST['genre'] = $record['genre'];
    $_POST['phonenumber'] = $record['phonenumber'];
    $_POST['class'] = $record['class'];
    $_POST['modelid'] = $record['id'];
}

?>

<div class="model-update ilimiter">

    <section class="page-section enroll ">
    <div class="container enroll">
        <div class="row model-create ilimiter">
            <div class="col-md-6 m-auto">
                <div class="notification <?= $class ?? "" ?>">

                    <?php

                    foreach ($messages as $message) {
                        echo "<p><i></i>$message</p>";
                    }

                    ?>

                </div>

                <form method="POST" action="?model=applyants&page=edit&modelid=<?=$id?>" >

                    <div>
                        <input type="hidden" id="modelid" name="modelid" value="<?= $_POST['modelid'] ?>">
                    </div>
                    
                    <?php include 'pages/applyants/fields.php';   ?>

                    <div>
                        <input type="hidden" id="action" name="action" value="create">
                    </div>

                    <div class="form-group input-actions">
                        <div class="form-group"><button class="btn btn-success" type="submit" value="update" class="button submit">Actualiar</button></div>
                        <div class="form-group"><button class="btn" style="background-color:gray;color:#fff;" type="reset" class="button reset">Limpiar </button></div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</section>