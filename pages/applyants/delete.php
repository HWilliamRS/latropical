<?php

$deleteResult = false;
$class = "error";
$messages = [];
$record = [];

if (isset($_POST['action'])  && ($_POST['action'] == 'delete')) {


    $result = deleteRecord("applyants", $_POST['modelid']);

    $deleteResult = true;
    $class = "success";

    $messages = [
        "El registro fue borrado de manera exitosa. 
        <a href='?model=applyants&page=list'>Ir a aplicantes<a/>"
    ];
} else {

    $record = selectRecordByID("applyants", $_GET['modelid']);

    $messages = ["        
        ¿Estás seguro que deseas eliminar este registro? 
        <a href='?model=applyants&page=list'>cancelar<a/>
        <button type='submit' value='delete'class='btn'>Eliminar</button>
        <input type='hidden' id='modelid' name='modelid' value='" .  $_GET['modelid'] . "' />
        <input type='hidden' id='action' name='action' value='delete' />
    "];
}

?>

<section class="page-section enroll ">
    <div class="model-show ilimiter">

        <div class="register-delete">
            <div class="notification <?= $class ?? "" ?>">
                <form method='POST' class="delete">
                    <?php

                    foreach ($messages as $message) {
                        echo "<p><i></i>$message</p>";
                    }

                    ?>
                </form>
            </div>
            
            <?php if (!$deleteResult) : ?>
            <div class="card">
                <div class="header-card">
                    <div class="title-card"><span>Aplicantes</span></div>

                </div>

                
                    <div class="info-card">

                        <?php include 'pages/applyants/card_fields.php'; ?>

                    </div>
                

            </div>
            <?php endif; ?>

        </div>
</section>