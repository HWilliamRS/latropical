<div class="card-fields">
    <div class="info-span"><span> ID </span></div>
    <div class="info-value"><span><?= $record['id']  ?></span></div>
</div>
<div class="card-fields">
    <div class="info-span"><span> Nombrés </span></div>
    <div class="info-value"><span><?= $record['firstname']  ?></span></div>
</div>
<div class="card-fields">
    <div class="info-span"><span> Apellidos </span></div>
    <div class="info-value"><span><?= $record['lastname']  ?></span></div>
</div>
<div class="card-fields">
    <div class="info-span"><span> Número de telefono </span></div>
    <div class="info-value"><span><?= $record['phonenumber']  ?></span></div>
</div>
<div class="card-fields">
    <div class="info-span"><span> Género </span></div>
    <div class="info-value"><span><?= $record['genre']  ?></span></div>
</div>
<div class="card-fields">
    <div class="info-span"><span> Clase </span></div>
    <div class="info-value"><span><?= $record['class']  ?></span></div>
</div>