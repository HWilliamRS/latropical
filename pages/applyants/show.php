<?php

$record = selectRecordByID("applyants",$_GET['modelid']);

?>

<div class="model-show ilimiter">

    <div clas="register-show">

        <div class="card">
            <div class="header-card">
                <div class="title-card"><span>Aplicantes</span></div>
                <div class="actions-card">
                    <a class="tooltip" href="?model=applyants&page=edit&modelid=<?= $record['id'] ?>">
                        <span class="tooltiptext">editar</span>
                        <i class="far fa-edit fa-2x"></i>
                    </a>

                    <a class="tooltip" href="?model=applyants&page=delete&modelid=<?= $record['id'] ?>">
                        <span class="tooltiptext">Eliminar</span>
                        <i class="far fa-trash-alt fa-2x"></i>
                    </a>
                </div>
            </div>
            <div class="info-card">
                
                <?php include 'pages/applyants/card_fields.php'; ?>

            </div>

        </div>

    </div>

</div>