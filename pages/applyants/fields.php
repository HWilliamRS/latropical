<div class="form-group">
    <label for="phonenumber"><span class="req">* </span> Número telefonico: </label>
    <input requirel type="text" name="phonenumber" id="phonenumber" class="form-control phone" maxlength="10" onkeyup="verifyPhone()" value="<?= $_POST['phonenumber'] ?? "" ?>" />
    <div id="errPhone"></div>
</div>

<div class="form-group">
    <label for="firstname"><span class="req">* </span> Nombres: </label>
    <input class="form-control" type="text" name="firstname" id="firstname" requirel value="<?= $_POST['firstname'] ?? "" ?>" />
    <div id="errFirst"></div>
</div>

<div class="form-group">
    <label for="lastname"><span class="req">* </span> Apellidos: </label>
    <input class="form-control" type="text" name="lastname" id="txt" requirel value="<?= $_POST['lastname'] ?? "" ?>" />
    <div id="errLast"></div>
</div>

<div class="form-group">
    <label for="genre"><span class="req">* </span> Género: </label>
    <select name="genre" id="genre" class="form-control">
        <option value=""> -- Seleccionar -- </option>
        <option value="f">Femenino</option>
        <option value="m">Masculino</option>
    </select>
</div>

<div class="form-group">
    <label for="email"><span class="req">* </span> Clase: </label>
    <select name="class" id="class" class="form-control">
        <option value=""> -- Seleccionar -- </option>
        <option value="salsa">Salsa</option>
        <option value="merengue">Merengue</option>
        <option value="bachata">Bachata</option>
    </select>
</div>

