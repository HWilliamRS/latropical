<?php

 $records = selectAll("applyants");

?>

<div class="model-list ilimiter">

    <div class="header-card">
        <div class="title-card"><span>Aplicantes</span></div>
        <div class="actions-card">
                <a class="btn-tooltip" style="margin-right:70px;" href="?model=applyants&page=create">
                    <span class="btn-tooltiptext">Agregar nuevo</span>
                    <i class="far fa-plus-square fa-2x"></i>
                </a>
        </div>
    </div>
</div>

<div class="data-grid">
    <div class="table100-head">
        <table>
            <thead>
                <tr class="table100-head">
                    <th class="column1">ID</th>
                    <th class="column2">Nombres</th>
                    <th class="column3">Apellidos</th>
                    <th class="column4">Teléfono</th>
                    <th class="column5">Correo electrónico</th>
                    <th class="column6">Sexo</th>
                    <th class="column7">Clase</th>
                    <th class="column8">Acciones</th>
                </tr>
            </thead>
            <tbody>

                <?php foreach ($records as $record) : ?>
                    <tr>
                        <td class="column1"><?= $record['id'] ?></td>
                        <td class="column2"><?= $record['firstname'] ?></td>
                        <td class="column3"><?= $record['lastname'] ?></td>
                        <td class="column4"><?= $record['phonenumber'] ?></td>
                        <td class="column5"><?= $record['email'] ?></td>
                        <td class="column6"><?= $record['genre'] ?></td>
                        <td class="column7"><?= $record['class'] ?></td>
                        <td>
                            <a class="btn-tooltip" href="?model=applyants&page=show&modelid=<?= $record['id'] ?>">
                                <span class="btn-tooltiptext">Ver</span>
                                <i class="far fa-file-alt fa-2x"></i>
                            </a>

                            <a class="btn-tooltip" href="?model=applyants&page=edit&modelid=<?= $record['id'] ?>">
                                <span class="btn-tooltiptext">editar</span>
                                <i class="far fa-edit fa-2x"></i>
                            </a>

                            <?php ?>
                                <a class="btn-tooltip" href="?model=applyants&page=delete&modelid=<?= $record['id'] ?>">
                                    <span class="btn-tooltiptext">Eliminar</span>
                                    <i class="far fa-trash-alt fa-2x"></i>
                                </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>


</div>