<?php

include $_SERVER['DOCUMENT_ROOT'] . '/config/database.php';
include $_SERVER['DOCUMENT_ROOT'] . '/functions/functions.php';


if (isset($_POST['email']) && isset($_POST['password'])) {

    $email = $_POST['email'];
    $password = md5($_POST['password']);

    $conn = newDBConn();

    $sql = "SELECT * FROM users WHERE email like '$email' AND password like '$password' ";

    $result = $conn->query($sql);

    $record = [];

    if ($result->num_rows > 0) {

        $record =  $result->fetch_assoc();

        session_start();
        $_SESSION['email'] = $record['email'];
        $_SESSION['user'] = $record['user'];

        /*
        $_SESSION['roles'] =  $record['roles'];
        */

        header("Location: /");
    } else {
        $message = "Credenciales incorrectas!";
    }
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Escuela de baile ,'La tropical latina'</title>


    <!-- Custom styles for our site -->
    <link href="/css/main.css" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link href="/css/thirdparty/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/business-casual.min.css" rel="stylesheet">

    <link href="/css/fonts/fontawesome-5.11.2/css/all.css" rel="stylesheet" type="text/css" />


</head>

<body id="login-container">

    <section class="page-section about-heading">
        <div class="container">
            <img class="img-fluid rounded about-heading-img mb-3 mb-lg-0" src="/img/login.jpg" alt="">
            <div class="about-heading-content">
                <div class="row">
                    <div class="col-xl-9 col-lg-10 mx-auto login-container">
                        <div class="bg-faded rounded p-5 log-form">
                            <h2 class="section-heading mb-4">
                                <span class="section-heading-upper">Sistema de Gestión de Aplicantes</span>
                                <span class="section-heading-lower">Ingresa tus credenciales</span>
                            </h2>

                            <?php if (isset($message)) : ?>
                                <div class="notification error">
                                    <p>Credenciales invalidas</>
                                </div>
                            <?php endif ?>
                            <form method="post">
                                <input type="text" id="email" name="email" title="username" placeholder="Correo electrónico" />
                                <input type="password" id="password" name="password" title="password" placeholder="Contraseña" />
                                <button type="submit" class="btn">Acceder</button>
                                <a class="forgot" href="#">¿Olvidaste tu contraseña?</a>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</body>

</html>