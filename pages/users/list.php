<?php

$records = selectAll("users");

?>

<div>

    <div class="model-list ilimiter">

        <div class="header-card">
            <div class="title-card"><span>Usuarios</span></div>
            <div class="actions-card">
                <a class="btn-tooltip" style="margin-right:70px;" href="?model=users&page=create">
                    <span class="btn-tooltiptext">Agregar nuevo</span>
                    <i class="far fa-plus-square fa-2x"></i>
                </a>
            </div>
        </div>
    </div>

    <div class="data-grid">
        <div class="table100-head">
            <table>
                <thead>
                    <tr class="table100-head">
                        <th class="column1">ID</th>
                        <th class="column2">Nombre de usuario</th>
                        <th class="column3">Correo electrónico</th>
                        <th class="column8">Acciones</th>
                    </tr>
                </thead>
                <tbody>

                    <?php foreach ($records as $record) : ?>
                        <tr>
                            <td class="column1"><?= $record['id'] ?></td>
                            <td class="column2"><?= $record['username'] ?></td>
                            <td class="column3"><?= $record['email'] ?></td>
                            <td>
                                <a class="btn-tooltip" href="?model=users&page=show&modelid=<?= $record['id'] ?>">
                                    <span class="btn-tooltiptext">Ver</span>
                                    <i class="far fa-file-alt fa-2x"></i>
                                </a>

                                <a class="btn-tooltip" href="?model=users&page=edit&modelid=<?= $record['id'] ?>">
                                    <span class="btn-tooltiptext">editar</span>
                                    <i class="far fa-edit fa-2x"></i>
                                </a>

                                <?php ?>
                                <a class="btn-tooltip" href="?model=users&page=delete&modelid=<?= $record['id'] ?>">
                                    <span class="btn-tooltiptext">Eliminar</span>
                                    <i class="far fa-trash-alt fa-2x"></i>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>


</div>