<div class="card-fields">
    <div class="info-span"><span> ID </span></div>
    <div class="info-value"><span><?= $record['id']  ?></span></div>
</div>
<div class="card-fields">
    <div class="info-span"><span> Nombre de usuario </span></div>
    <div class="info-value"><span><?= $record['username']  ?></span></div>
</div>
<div class="card-fields">
    <div class="info-span"><span> Correo electrónico </span></div>
    <div class="info-value"><span><?= $record['email']  ?></span></div>
</div>