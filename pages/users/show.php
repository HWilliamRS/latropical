<?php

$record = selectRecordByID("users",$_GET['modelid']);

?>

<div class="model-show ilimiter">

    <div clas="register-show">

        <div class="card">
            <div class="header-card">
                <div class="title-card"><span>Usuarios</span></div>
                <div class="actions-card">
                    <a class="tooltip" href="?model=users&page=edit&modelid=<?= $record['id'] ?>">
                        <span class="tooltiptext">editar</span>
                        <i class="far fa-edit fa-2x"></i>
                    </a>

                    <a class="tooltip" href="?model=users&page=delete&modelid=<?= $record['id'] ?>">
                        <span class="tooltiptext">Eliminar</span>
                        <i class="far fa-trash-alt fa-2x"></i>
                    </a>
                </div>
            </div>
            <div class="info-card">
                
                <?php include 'pages/users/card_fields.php'; ?>

            </div>

        </div>

    </div>

</div>