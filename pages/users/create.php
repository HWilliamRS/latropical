<?php

$fields = [];
$messages = [];

if (isset($_POST['action'])) {

    $response = validateUserFields();

    $fields = $response[0];
    $messages = $response[1];

    if(sizeof($messages) == 0 ){
        
        $fields['password'] = md5($fields['pass1']);
        unset($fields['pass1']);
        unset($fields['pass2']);
        $result = createRecord("users" , $fields);
    }else{
        $result = false;
    }
    if($result){
        $messages = ["Registro fue creado de manera exitosa!"];
        $class = "success";
        $_POST = array();
    }else {
        $class = "error";
    }

}

?>
<section class="page-section enroll ">
    <div class="container enroll">
        <div class="row model-create ilimiter">
            <div class="col-md-6 m-auto">
                <div class="notification <?= $class ?? "" ?>">

                    <?php

                    foreach ($messages as $message) {
                        echo "<p><i></i>$message</p>";
                    }

                    ?>

                </div>

                <form method="POST" action="?model=users&page=create" >

                    <?php include 'pages/users/fields.php';   ?>

                    <div>
                        <input type="hidden" id="action" name="action" value="create">
                    </div>

                    <div class="form-group input-actions">
                        <div class="form-group"><button class="btn btn-success" type="submit" value="create" class="button submit">Crear</button></div>
                        <div class="form-group"><button class="btn" style="background-color:gray;color:#fff;" type="reset" class="button reset">Limpiar </button></div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</section>