<div class="form-group">
    <label for="username"><span class="req">* </span> Nombre de usuario: <small>Este será su usuario de acceso.</small> </label>
    <input class="form-control" type="text" name="username" id="username" placeholder="Mínimo 6 caracteres" requirel value="<?= $_POST['username'] ?? "" ?>" />
    <div id="errLast"></div>
</div>

<div class="form-group">
    <label for="email"><span class="req">* </span> Correo electrónico: </label>
    <input class="form-control" requirel type="email" name="email" id="email" onblur="verifyEmail()" value="<?= $_POST['email'] ?? "" ?>" />
    <div class="errEmail" id="errEmail"></div>
</div>

<div class="form-group">
    <label for="password"><span class="req">* </span> Contraseña: </label>
    <input requirel name="pass1" type="password" class="form-control inputpass" minlength="4" maxlength="16" id="pass1" /> </p>

    <label for="password"><span class="req">* </span> Confirmar contraseña: </label>
    <input requirel name="pass2" type="password" class="form-control inputpass" minlength="4" maxlength="16" onkeyup="verifyPass()" placeholder="Introduzca nuevamente para validar" id="pass2" />
    <span id="confirmMessage" class="confirmMessage"></span>
</div>