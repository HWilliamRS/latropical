<?php

$responses = validateApplyantFields();


$fields = $responses[0] ;
$messages = $responses[0];


if(isset($_POST['enroll'])){

    echo "Guardando en la base de datos!";
    $servername = "localhost";
    $username = "latropicaldb";
    $password = "latropicaldb";
    $dbname = "latropicaldb";

    $table= "applyants";

    $conn = new mysqli($servername, $username, $password, $dbname);
    $conn->set_charset('utf8');

    
    if($conn->connect_error){
        die("Conexión fallida!");
    }

    if( count($messages) == 0){

        $columns = "";
        $values = ""; 

        $result = true;

        foreach($fields  as $column => $value){
            $columns .= $column . ',';
            $values .= "'". $value . "'" . ",";
        }

        $columns = substr($columns, 0, -1);
        $values = substr($values, 0, -1);

        $sql = "INSERT INTO $table  ($columns) VALUES ($values)";

        if($conn->query($sql) === FALSE ) {
            echo "Error: " . $sql . "<br/>" . $conn->error . "<br/>";
            $result = false;
        }

        $conn->close();
        echo " SQL :"  . $sql;

        echo "resultado de la operación : " . $result;

    }
   
}



?>

<section class="page-section enroll">
    <div class="container enroll">
        <div class="row">
            <div class="col-md-6 m-auto">
                <div class="notification <?= $class ?? "" ?>">

                    <?php

                    foreach ($messages as $message) {
                        echo "<p><i></i>$message</p>";
                    }

                    ?>

                </div>

                <form method="POST" id="enrollForm" role="form"  >
                    <fieldset>
                        <legend class="text-center">Información requerida para la inscripción <span class="req"><small> requerido *</small></span></legend>

                        <div class="form-group">
                            <label for="phonenumber"><span class="req">* </span> Número telefonico: </label>
                            <input requirel type="text" name="phonenumber" id="phonenumber" class="form-control phone" maxlength="10" onkeyup="verifyPhone()" value="<?= $_POST['phonenumber']??"" ?>"  />
                            <div id="errPhone"></div>
                        </div>

                        <div class="form-group">
                            <label for="firstname"><span class="req">* </span> Nombres: </label>
                            <input class="form-control" type="text" name="firstname" id="firstname" requirel value="<?= $_POST['firstname']??"" ?>" />
                            <div id="errFirst"></div>
                        </div>

                        <div class="form-group">
                            <label for="lastname"><span class="req">* </span> Apellidos: </label>
                            <input class="form-control" type="text" name="lastname" id="txt" requirel value="<?= $_POST['lastname']??"" ?>" />
                            <div id="errLast"></div>
                        </div>

                        <div class="form-group">
                            <label for="genre"><span class="req">* </span> Género: </label>
                            <select name="genre" id="genre" class="form-control" >
                                <option value=""> -- Seleccionar -- </option>
                                <option value="f" >Femenino</option>
                                <option value="m">Masculino</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="email"><span class="req">* </span> Correo electrónico: </label>
                            <input class="form-control" requirel type="email" name="email" id="email" onblur="verifyEmail()" value="<?= $_POST['email']??"" ?>"  />
                            <div class="errEmail" id="errEmail"></div>
                        </div>
<!-- 
                        <div class="form-group">
                            <label for="username"><span class="req">* </span> Nombre de usuario: <small>Este será su usuario de acceso.</small> </label>
                            <input class="form-control" type="text" name="username" id="username" placeholder="Mínimo 6 caracteres" requirel value="<?= $_POST['username']??"" ?>"  />
                            <div id="errLast"></div>
                        </div> -->

                        <div class="form-group">
                            <label for="email"><span class="req">* </span> Clase: </label>
                            <select name="class" id="class" class="form-control" >
                                <option value=""> -- Seleccionar -- </option>
                                <option value="salsa">Salsa</option>
                                <option value="merengue">Merengue</option>
                                <option value="bachata">Bachata</option>
                            </select>
                        </div>

                        <!-- <div class="form-group">
                            <label for="password"><span class="req">* </span> Contraseña: </label>
                            <input requirel name="pass1" type="password" class="form-control inputpass" minlength="4" maxlength="16" id="pass1"   /> </p>

                            <label for="password"><span class="req">* </span> Confirmar contraseña: </label>
                            <input requirel name="pass2" type="password" class="form-control inputpass" minlength="4" maxlength="16" onkeyup="verifyPass()" placeholder="Introduzca nuevamente para validar" id="pass2" />
                            <span id="confirmMessage" class="confirmMessage"></span>
                        </div> -->



                        <div class="form-group">
                            <input class="btn btn-success" id='action' name='action' type="submit"  name="submit_reg" value="Inscribirse">
                        </div>

                    </fieldset>
                </form><!-- ends register form -->

            </div><!-- ends col-6 -->

        </div>
    </div>
</section>

<script>
    var validColor = "#66cc66";
    var invalidColor = "#ff6666";
    

   function  validateForm(form){

        var submit = true

        if(!verifyPhone()){
            submit = false;
        }

        if(!verifyEmail()){
            submit = false;
        }

        if(!verifyPass()){
            submit = false;
        }  

        if(submit){
            document.getElementById('enrollForm').submit();
        }

    } 

    function verifyPass() {

        var pass1 = document.getElementById('pass1');
        var pass2 = document.getElementById('pass2');

        var message = document.getElementById('confirmMessage');


        var isValid = false;


        if (pass1.value == pass2.value) {

            pass2.style.backgroundColor = validColor;
            message.style.color = validColor;
            message.innerHTML = "Contraseñas válidas."
            isValid = true;
        } else {

            pass2.style.backgroundColor = invalidColor;
            message.style.color = invalidColor;
            message.innerHTML = "Las contraseñas introducidas no coninciden!";
            isValid = false
        }

        return isValid;

    }

    function verifyPhone() {
        var isValid = false;

        var phonenumber = document.getElementById('phonenumber');
        var message = document.getElementById('errPhone');

        if (!isNaN(phonenumber.value)) {

            phonenumber.style.backgroundColor = validColor;
            message.style.color = validColor;
            message.innerHTML = "";
            isValid = true;

        } else {

            phonenumber.style.backgroundColor = invalidColor;
            message.style.color = invalidColor;
            message.innerHTML = "El teléfono introducido es inválido.";
            isValid = false

        }

        return isValid;
    }


    function verifyEmail() {

        var isValid = false;
        var regMail = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;

        var email = document.getElementById('email');
        var message = document.getElementById('errEmail');

        if (regMail.test(email.value) == true) {
            email.style.backgroundColor = validColor;   
            message.style.color = validColor;        
            message.innerHTML = "Correo electrónico válido";
            isValid = true;
        } else {
            email.style.backgroundColor = invalidColor; 
            message.style.color = invalidColor; 
            message.innerHTML = "Correo electrónico inválido";
            isValid = false;
            
        }

        return isValid;
    }

</script>