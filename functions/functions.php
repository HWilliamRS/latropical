<?php

function validateApplyantFields()
{


    $messages = [];
    $fields = [];


    $responses = array($fields, $messages);

    if (isset($_POST['action'])) {

        if (isset($_POST['firstname']) && !empty($_POST['firstname'])) {
            $fields['firstname'] =   htmlspecialchars($_POST['firstname']);
        } else {
            $messages[] = "El campo 'Nombres' es requerido!";
        }

        if (isset($_POST['lastname']) && !empty($_POST['lastname'])) {
            $fields['lastname'] = htmlspecialchars($_POST['lastname']);
        } else {
            $messages[] = "El campo 'Apellidos' es requerido!";
        }

        if (isset($_POST['genre']) && !empty($_POST['genre'])) {
            $fields['genre'] = $_POST['genre'];
        } else {
            $messages[] = "El campo 'Sexo' es requerido!";
        }

        if (isset($_POST['phonenumber']) && !empty($_POST['phonenumber'])) {

            if (is_numeric($_POST['phonenumber']) && (strlen($_POST['phonenumber']) == 10)) {
                $fields['phonenumber'] = htmlspecialchars($_POST['phonenumber']);
            } else {
                $messages[] = "El campo 'Telefono' es invalido!";
            }
        } else {
            $messages[] = "El campo 'Telefono' es requerido!";
        }

        if (isset($_POST['class']) && !empty($_POST['class'])) {
            $fields['class'] =   htmlspecialchars($_POST['class']);
        } else {
            $messages[] = "El campo 'Clase' es requerido!";
        }

    }



    return array($fields, $messages);
}


function validateUserFields()
{
    $messages = [];
    $fields = [];

    $responses = array($fields, $messages);

    if (isset($_POST['action'])) {

        
        if (isset($_POST['username']) && !empty($_POST['username'])) {
            $fields['username'] =   htmlspecialchars($_POST['username']);
        } else {
            $messages[] = "El campo 'Nombre de usuario' es requerido!";
        }

        if (isset($_POST['email']) && !empty($_POST['email'])) {

            if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                $fields['email'] = htmlspecialchars($_POST['email']);
            } else {
                $messages[] = "El campo 'Correo electrónico' es invalido!";
            }
        } else {
            $messages[] = "El campo 'Correo electrónico' es requerido!";
        }

        if (isset($_POST['pass1']) && !empty($_POST['pass1'])) {
            $fields['pass1'] =   htmlspecialchars($_POST['pass1']);

            if (isset($_POST['pass2']) && !empty($_POST['pass2'])) {
                $fields['pass2'] =   htmlspecialchars($_POST['pass2']);

                if ($fields['pass1'] != $fields['pass2']) {
                    $messages[] = "El campo 'Repite contraseña' no coincide con la 'contraseña'!";
                }
            } else {
                $messages[] = "El campo 'Repite contraseña' es requerido!";
            }
        } else {
            $messages[] = "El campo 'Contraseña' es requerido!";
        }

        
    }

    return array($fields, $messages);
}
