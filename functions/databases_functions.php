<?php


function selectAll($table){
    $conn = newDBConn();

    $sql = "SELECT * FROM $table WHERE active = 1";
    $results = $conn->query($sql);

    $records = [];

    if($results->num_rows > 0 ){

        while($record = $results->fetch_assoc()){
            $records[] = $record;
        }

    }

    $conn->close();

    return $records;
}

function selectRecordByID($table, $id)
{

    $conn = newDBConn();

    $sql = "SELECT * FROM $table where id = " . $id;
    $result = $conn->query($sql);

    $record = [];

    if ($result->num_rows > 0) {

        $record =  $result->fetch_assoc();
    }

    $conn->close();

    return $record;
 }

 function createRecord($table, $fields)
{
    $conn = newDBConn();

    $columns = "";
    $values = "";

    $result = true;

    foreach ($fields as $column => $value) {
        $columns .= $column . ',';
        $values .= "'" .  $value . "'" . ",";
    }

    $columns = substr($columns, 0, -1);
    $values = substr($values,  0, -1);

    $sql = "INSERT INTO $table ($columns) VALUES ($values)";

    if ($conn->query($sql) === FALSE) {
        echo "Error: " . $sql . "<br>" . $conn->error;
        $result = false;
    }

    $conn->close();

    return $result;
}

function updateRecord($table, $id, $fields)
{
    $conn = newDBConn();

    $columnsValues = "";

    $result = true;

    foreach ($fields as $column => $value) {
        $columnsValues .= $column . ' = ' . "'" .  $value . "'" . ',';
    }

    $columnsValues = substr($columnsValues, 0, -1);

    $sql = "UPDATE $table SET $columnsValues WHERE id=$id";

    if ($conn->query($sql) === FALSE) {
        echo "Error: " . $sql . "<br>" . $conn->error;
        $result = false;
    }

    $conn->close();

    return $result;
}


function deleteRecord($table, $id)
{ 
    $conn = newDBConn();

    $columnsValues = "";

    $result = true;

    $sql = "UPDATE $table SET active = 0 WHERE id=$id";

    if ($conn->query($sql) === FALSE) {
        echo "Error: " . $sql . "<br>" . $conn->error;
        $result = false;
    }

    $conn->close();

    return $result;

}
