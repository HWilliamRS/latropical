<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Escuela de baile ,'La tropical latina'</title>


  <!-- Custom styles for our site -->
  <link href="css/main.css" rel="stylesheet">

  <!-- Bootstrap core CSS -->
  <link href="css/thirdparty/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/business-casual.min.css" rel="stylesheet">

  <link href="css/fonts/fontawesome-5.11.2/css/all.css" rel="stylesheet" type="text/css" />


</head>

<body>
  <?php
  //Comentario de una línea

  /* 
        Comentarios de mas de líneas

        */

  /**
   * 
   * Comentarios de documentación
   * 
   */
  ?>

  <!-- Inicio del header del sitio  -->

  <h1 class="site-heading text-center text-white d-none d-lg-block">
    <span class="site-heading-upper text-primary mb-3">Escuela de baile</span>
    <span class="site-heading-lower">La tropical</span>
  </h1>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark py-lg-4" id="mainNav">
    <div class="container">
      <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none" href="#">Start Bootstrap</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav mx-auto">
          <li class="nav-item px-lg-4 <?= ((!isset($_GET['page'])) || (isset($_GET['page']) && ($_GET['page'] == 'home'))) ? 'active' : ''; ?>">
            <a class="nav-link text-uppercase text-expanded" href="?page=home">Inicio
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item px-lg-4 <?= (isset($_GET['page']) && ($_GET['page'] == 'about')) ? 'active' : ''; ?>">
            <a class="nav-link text-uppercase text-expanded" href="?page=about">Sobre la escuela</a>
          </li>
          <li class="nav-item px-lg-4 <?= (isset($_GET['page']) && ($_GET['page'] == 'class')) ? 'active' : ''; ?>">
            <a class="nav-link text-uppercase text-expanded" href="?page=class">Clases</a>
          </li>
          <li class="nav-item px-lg-4 <?= (isset($_GET['page']) && ($_GET['page'] == 'store')) ? 'active' : ''; ?>">
            <a class="nav-link text-uppercase text-expanded" href="?page=store">Tienda</a>
          </li>
          <li class="nav-item px-lg-4 <?= (isset($_GET['page']) && ($_GET['page'] == 'enroll')) ? 'active' : ''; ?>">
            <a class="nav-link text-uppercase text-expanded" href="?page=enroll">Inscripción</a>
          </li>
          <?php if (isset($_SESSION['email'])) : ?>
            <li class="nav-item px-lg-4 <?= (isset($_GET['model']) && ($_GET['model'] == 'applyants')) ? 'active' : ''; ?>">
              <a class="nav-link text-uppercase text-expanded" href="?page=list&model=applyants">Aplicantes</a>
            </li>
            <li class="nav-item px-lg-4 <?= (isset($_GET['model']) && ($_GET['model'] == 'users')) ? 'active' : ''; ?>">
              <a class="nav-link text-uppercase text-expanded" href="?page=list&model=users">Usuarios</a>
            </li>
            <li class="dropdown">
              <div class="user-menu">
                <div class="img"><i class="fas fa-user fa-2x"></i></div>
                <a class="username dropbtn" href="javascript:void(0)"><?= $_SESSION['email'] ?></a>
                <div class="dropdown-content">
                  <a href="#">Mi Perfil</a>
                  <a href="/pages/auth/logout.php">Cerrar sesión</a>
                </div>
              </div>

            </li>
          <?php else : ?>

            <li><a href="/pages/auth/login.php">Iniciar sesión</a></li>

          <?php endif ?>
        </ul>
      </div>
    </div>
  </nav>
  <!-- Fin del header del sitio  -->