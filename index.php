<?php
session_start();
include 'config/database.php';
include 'functions/databases_functions.php';
include 'functions/functions.php';



include 'header.php';

?>

<?php

//Verificar en el parametro GET si el indicé 'page' ha sido pasado
if (isset($_GET['page'])) {

    $route = (isset($_REQUEST['model'])) ? 'pages/' . str_replace('"', "", $_GET['model']) . '/' : 'pages/';

    //Escapar las comiilas que vienen con el valor de la variable
    $page = $route . str_replace('"', "", $_GET['page']) . ".php";

    //Verificar si el archivo que intentaremos cargar existe
    if (file_exists($page)) {
        include $page;
    } else {
        include 'pages/errors/404.php';
    }
} else {
    include "pages/home.php";
}
?>


?>

<?php

include 'footer.php';
?>