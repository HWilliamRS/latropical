-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 30, 2020 at 02:36 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `latropicaldb`
--
CREATE DATABASE IF NOT EXISTS `latropicaldb` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `latropicaldb`;

-- --------------------------------------------------------

--
-- Table structure for table `applyants`
--

CREATE TABLE `applyants` (
  `id` int(11) NOT NULL,
  `firstname` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `phonenumber` varchar(45) DEFAULT NULL,
  `genre` varchar(45) DEFAULT NULL,
  `class` varchar(45) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `applyants`
--

INSERT INTO `applyants` (`id`, `firstname`, `lastname`, `email`, `phonenumber`, `genre`, `class`, `active`) VALUES
(7, 'Jorge Félix', 'Gonzale Acosta', 'jfga@latropical.com', '8098550909', 'm', 'bachata', 1),
(9, 'Jesús Raynieri', 'Melo Maggiolo', 'jrmm@latropical.com', '8099871234', 'm', 'merengue', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `applyants`
--
ALTER TABLE `applyants`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `applyants`
--
ALTER TABLE `applyants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
