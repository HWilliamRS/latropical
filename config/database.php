<?php

function newDBConn()
{

    $servername = "localhost";
    $username = "latropicaldb";
    $password = "latropicaldb";
    $dbname = "latropicaldb";

    $table = "applyants";

    $conn = new mysqli($servername, $username, $password, $dbname);
    $conn->set_charset('utf8');


    if ($conn->connect_error) {
        die("Conexión fallida!");
    }

    return $conn;
}
